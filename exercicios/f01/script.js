var txtInicio = ""
var txtFim = ""
var passo = ""


function lerEntradas(){
    txtInicio = window.document.getElementById("txtInicio")
    txtFim = window.document.getElementById("txtFim")
    passo = window.document.getElementById("txtPasso")

}

function isEmpty(){
    if( txtInicio.value.length == 0 || txtFim.value.length == 0 || passo.value.length == 0){
        return true
    }else{
        return false
    }
}

function contar(){
    lerEntradas()
    var campoTxt1 = window.document.getElementById("resposta")

    if(isEmpty()){
        campoTxt1.innerHTML = "Preencha todos os campos "
    }else{
        campoTxt1.innerHTML = `Contando: <br>`
        let i = Number(txtInicio.value)
        let f = Number(txtFim.value)
        let p = Number(passo.value)

        if(p<=0){
            window.alert(`Passo com valor inválido ${p} considerando passo 1`)
            p=1
        }

        if(i < f ){
            for(let cta = i; cta <= f; cta += p){
                campoTxt1.innerHTML += `${cta}  \u{1F449}`
            }
        }else{
            for(let cta = i; cta >= f; cta -= p){
                campoTxt1.innerHTML += `${cta}  \u{1F449}`
            }
        }
        campoTxt1.innerHTML += `\u{1F3C1}`
    }
}