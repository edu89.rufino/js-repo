function gerarTabuada(){
    let numTxt = window.document.getElementById("num").value
    let resposta = window.document.getElementById("resultado")
    resposta.innerHTML = ""    
    if(numTxt.length == 0){
        window.document.getElementById("response").innerHTML = `Preencha o campo número`    
    }else {
        let num = Number(numTxt)
        if(num >= 0){
            for(let i = 0 ; i <=10 ; i++){
                let item = document.createElement('option')
                item.text= `${num} x ${i} = ${i*num}`
                item.value = `tab${i}`
                resposta.appendChild(item)
            }
        }else{
            window.document.getElementById("response").innerHTML = `Preencha o campo número com valor Naturais`
        }
    }
}