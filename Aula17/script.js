let num = []

function adicionar(){
    var valTxtInput = window.document.getElementById("txt_input").value.toString()
    var txtPresenter = window.document.getElementById("txt_presenter") 
        
    addArray(valTxtInput)
    txtPresenter.innerHTML = addTextArea(num)
    
    window.document.getElementById("txt_input").innerHTML = ""
}

function addArray(valor){
    if(num.indexOf(valor) < 0){
        num.push(valor)
    }
    return num
}

function addTextArea(num) {
    var s =""
    for(let n = 0 ; n < num.length ; n++){
        s += num[n].toString() + "\n" 
    }
    return s
}